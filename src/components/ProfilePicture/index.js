import { StyledProfile } from './index.style'
import profile from '../../assets/images/profile-pic.jpg'

export default function ProfilePicture() {
	return <StyledProfile src={profile} />
}
