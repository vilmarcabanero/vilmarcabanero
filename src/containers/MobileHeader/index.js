import { Container } from './index.style'
import MobileNavLinks from '../../components/MobileNavLinks'

export default function MobileHeader() {
	return (
		<Container>
			<MobileNavLinks />
		</Container>
	)
}
