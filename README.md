<img width="100%" height="100%" style="display:inline; border-radius:5px; box-shadow: 5px 5px 10px gray;" align="center" src="./banner.JPG" />

[//]: # 'IMG SHIELDS FROM: https://github.com/alexandresanlim/Badges4-README.md-Profile'

<h2><samp>👋 Hi</samp></h2>

<samp>
    <ul>
        <li><strong>What I'm currently building?</strong>
            <ul>
                <li>Entropiya<a href="https://github.com/vilmarcabanero/entropiya-client">📦</a></li>
                <li>Niventa<a href="https://github.com/vilmarcabanero/niventa-client">📦</a></li>
                <li>Portfolio <a href="https://github.com/vilmarcabanero/vilmarcabanero">📦</a></li>
            </ul>
        </li><br/>
        <li><strong>What I'm planning to study?</strong>
            <ul>
                <li>React Native (Mobile Apps) 📱</li>
                <li>Electron JS (Desktop Apps) 💻</li>
            </ul>
        </li>
    </ul>
</samp>

<h2><samp>📈 GITHUB STATISTICS</samp></h2>

<p align="center">
<img width="49%" heigth="100%" style="display:inline" align="center" src="https://github-readme-stats.vercel.app/api?username=vilmarcabanero&show_icons=true&line_height=27&count_private=true&theme=gotham&card_width=300&include_all_commits=true" />

<img width="49%" heigth="100%" style="display:inline" align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=vilmarcabanero&hide=shell,html,css,scss,sass=html&theme=gotham&langs_count=6&layout=compact" />
</p>

<h2><samp>💪 STRONG STACK</samp></h2>

<p>
    <img src="https://img.shields.io/badge/MongoDB-%234ea94b.svg?&style=for-the-badge&logo=mongodb&logoColor=white">
    <img src="https://img.shields.io/badge/express.js%20-%23404d59.svg?&style=for-the-badge">
    <img src="https://img.shields.io/badge/react%20-%2320232a.svg?&style=for-the-badge&logo=react&logoColor=%2361DAFB">
    <img src="https://img.shields.io/badge/node.js%20-%2343853D.svg?&style=for-the-badge&logo=node.js&logoColor=white">
</p>

<h2><samp>🎨 FRONTEND TECHNOLOGIES</samp></h2>

<p>
    <img src = "https://img.shields.io/badge/html-%23239120.svg?&style=for-the-badge&logo=html5&logoColor=white"> 
    <img src = "https://img.shields.io/badge/css-%23239120.svg?&style=for-the-badge&logo=css3&logoColor=white">
    <img src="https://img.shields.io/badge/sass%20-%23CC6699.svg?&style=for-the-badge&logo=sass&logoColor=white">
    <img src="https://img.shields.io/badge/bootstrap%20-%23563D7C.svg?&style=for-the-badge&logo=bootstrap&logoColor=white">
    <img src="https://img.shields.io/badge/material%20ui%20-%230081CB.svg?&style=for-the-badge&logo=material-ui&logoColor=white">
    <img src="https://img.shields.io/badge/javascript-%23F7DF1E.svg?&style=for-the-badge&logo=javascript&logoColor=black">
    <img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white">
    <img src="https://img.shields.io/badge/react%20-%2320232a.svg?&style=for-the-badge&logo=react&logoColor=%2361DAFB">
    <img src="https://img.shields.io/badge/react_router%20-CA4245.svg?&style=for-the-badge&logo=react-router&logoColor=white">
    <img src="https://img.shields.io/badge/styled_components%20-DB7093.svg?&style=for-the-badge&logo=styled-components&logoColor=white">
<p>

<h2><samp>💻 BACKEND TECHNOLOGIES</samp></h2>

<p>
    <img src="https://img.shields.io/badge/GraphQL%20-E10098?logo=graphql&logoColor=white&style=for-the-badge" />
    <img src="https://img.shields.io/badge/node.js%20-%2343853D.svg?&style=for-the-badge&logo=node.js&logoColor=white">
    <img src="https://img.shields.io/badge/express.js%20-%23404d59.svg?&style=for-the-badge">
    <img src="https://img.shields.io/badge/Next%20JS-000000?logo=next-dot-js.svg&logoColor=white&style=for-the-badge" />
<p>

<h2><samp>🙊 DATABASE MANAGEMENT SYSTEM</samp></h2>

<p>
    <img src="https://img.shields.io/badge/MongoDB-%234ea94b.svg?&style=for-the-badge&logo=mongodb&logoColor=white">
<p>

<h2><samp>🌏 SERVER</samp></h2>

<p>
    <img src="https://img.shields.io/badge/heroku%20-430098.svg?&style=for-the-badge&logo=heroku&logoColor=white">
    <img src="https://img.shields.io/badge/netlify%20-00C7B7.svg?&style=for-the-badge&logo=netlify&logoColor=white">
<p>

<h2><samp>🔧 DEVELOPMENT TOOLS</samp></h2>

<p>
    <img src="https://img.shields.io/badge/Jest%20-C21325?logo=jest&logoColor=white&style=for-the-badge" />
    <img src="https://img.shields.io/badge/Postman%20-FF6C37?logo=postman&logoColor=white&style=for-the-badge" />
    <img src="https://img.shields.io/badge/Docker%20-2496ED?logo=docker&logoColor=white&style=for-the-badge" />
    <img src="https://img.shields.io/badge/Webpack-%238DD6F9.svg?&style=for-the-badge&logo=webpack&logoColor=white">
    <img src="https://img.shields.io/badge/Gulp-%23CF4647.svg?&style=for-the-badge&logo=gulp&logoColor=white">
    <img src="https://img.shields.io/badge/Yarn%20-2C8EBB?logo=yarn&logoColor=white&style=for-the-badge" />
    <img src="https://img.shields.io/badge/Git%20-F05032?logo=git&logoColor=white&style=for-the-badge" />
    <img src="https://img.shields.io/badge/github-%23100000.svg?&style=for-the-badge&logo=github&logoColor=white">
    <img src="https://img.shields.io/badge/NPM%20-CB3837?logo=npm&logoColor=white&style=for-the-badge" />
<p>
